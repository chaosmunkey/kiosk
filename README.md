# Monitoring Kiosk

The _monitoring kiosk_ is a simple **Python** script which utilises **Selenium** to automate the browsing of web pages, and where necessary, refreshing/reloading.

### Prerequisites
 * Python 2.7 (Might work with Python 3, but is untested.)
 * Selenium >= 3.141.0
 * Google Chrome Browser
 * Google Chrome Driver

### Installation
##### Ubuntu
_Following commands to be run as **sudo**:_
 1. `wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -`
 2. `echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google-chrome.list`
 3. `apt update && apt install -y curl google-chrome-stable python-pip`
 4. `pip install -U pip selenium`
 5. `curl -o /tmp/chromedriver.zip https://chromedriver.storage.googleapis.com/2.44/chromedriver_linux64.zip`
 6. `unzip /tmp/chromedriver.zip -d /tmp`
 7. `cp /tmp/chromedriver /usr/local/bin`


##### MacOSX
_**Todo**_


### Usage
##### Configuration
A configuration file called `web_pages.json` is required by the the `kiosk` to run. As it currently stands, this file needs to be located in the same directory as `kiosk.py`.

###### Structure
```
{
    "web_pages": [
        {
            "uri": "http://www.duckduckgo.com",
            "reload": true,
            "reload_time": 10,
            "display_for": 15
        },
        ....
    ]
}
```

|tag|description|
|:-:|:-:|
|**uri**|Website to serve.|
|**reload**|_true_ if page requires a reload to show updated content, else _false_.|
|**reload_time**|Time (in seconds) required for the page to reload, prevents the page from being rotated too soon.|
|**display_for**| Time (in seconds) to display the page before rotating.|


##### Running the `kiosk`
Simply run the following command from the directory the software has installed into:
```
python main.py
```

----

### Future Improvements
* [x] Allow rotation to be paused.
* [ ] Allow auto login of pages.
* [ ] Allow sites to be added/removed when config file updates.
* [ ] Specifying of config file.
* [ ] Make it _chef_-able.
* [ ] Runnable on MacOS (and maybe Windows).
