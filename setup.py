from setuptools import setup

classifiers = [
    "Programming Language :: Python :: 2.7"
]


setup(name="kiosk",
     version="0.0.3",
     author="Tom Winn",
     author_email="tom_winn@mac.com",
     classifiers=classifiers,
     packages=["kiosk"],
     scripts=["bin/kiosk"],
     install_requires=[
         "selenium"])
