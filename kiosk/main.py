#!/usr/bin/python


# STDLIB IMPORTS
from sys import exit

# THIRD PARTY IMPORTS
from gi import require_version
require_version('Gtk', '3.0')
from gi.repository import Gtk

# LOCAL IMPORTS
from KioskUI import KioskUI


def main():
    try:
        KioskUI()
        Gtk.main()
    except KeyboardInterrupt:
        print
        exit(0)
    except IOError as e:
        print(e)
        exit(-1)
    except ValueError as e:
        print(e)
        exit(-1)


if __name__ == "__main__":
    main()
