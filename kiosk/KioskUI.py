#!/usr/bin/python


# THIRD PARTY IMPORTS
from gi.repository import Gtk

# LOCAL IMPORTS
from Kiosk import Kiosk


class KioskUI(Gtk.Window):
    """
    Class to provide a UI for the Kiosk
    """

    def __init__(self):
        """
        Instantiate a class of KioskUI.
        """
        super(KioskUI, self).__init__(title="Monitoring Kiosk")
        self.connect("destroy", Gtk.main_quit)
        self.set_default_size(350, 50)
        # self.set_resizable(False)
        self.set_border_width(10)

        self._content_area = Gtk.Box(spacing=5)
        self.add(self._content_area)

        self._btn_start_pause = Gtk.Button.new_with_label("Start")
        self._btn_start_pause.connect("clicked", self._cb_start_pause_kiosk)
        self._content_area.pack_start(self._btn_start_pause, True, True, 0)

        self.show_all()

        self._kiosk_started = False
        self._kiosk = Kiosk()

    def _cb_start_pause_kiosk(self, data):
        """
        Callback for 'btn_start_pause_kiosk'.
        """
        if self._kiosk_started:
            success = self._kiosk.stop_kiosk()
            self._kiosk_started = False
        else:
            success = self._kiosk.start_kiosk()
            self._kiosk_started = success

        if success:
            if self._kiosk_started:
                self._btn_start_pause.set_label("Pause")
            else:
                self._btn_start_pause.set_label("Start")
