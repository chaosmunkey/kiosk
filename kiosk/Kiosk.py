#!/usr/bin/python


# STDLIB IMPORTS
from os import makedirs
from os.path import dirname, exists, expanduser, join
from shutil import copy
from threading import Thread
from time import sleep, time
import json

# THIRD PARTY IMPORTS
from selenium import webdriver


class Kiosk:
    """
    Kiosk...
    """

    CONFIG_DIR = join(expanduser('~'), ".kiosk")
    PAGES_CONFIG = join(CONFIG_DIR, "web_pages.json")
    EXAMPLE_FILE = join(dirname(__file__), "web_pages-example.json")

    def __init__(self):
        """
        Initialise an instance of Kiosk.
        """
        self._config = self._read_config()

        driver_opts = self._get_driver_options()
        self._driver = webdriver.Chrome(chrome_options=driver_opts)

        self._running = False
        self._run_it_thread = None

    def _read_config(self):
        """
        Read the config file. If it doesn't exist, copy the example
        to the user's home directory.
        """
        try:
            if not exists(self.CONFIG_DIR):
                makedirs(self.CONFIG_DIR)

            if not exists(self.PAGES_CONFIG):
                copy(self.EXAMPLE_FILE, self.PAGES_CONFIG)

            with open(self.PAGES_CONFIG) as fh:
                data = json.load(fh)
        except IOError as err:
            print(err)
            exit(-1)

        return data

    @staticmethod
    def _get_driver_options():
        """
        Return the options to be used by the web driver
        """
        options = webdriver.ChromeOptions()
        options.add_argument('--kiosk')
        # options.binary_location = "/usr/bin/google-chrome"

        return options

    def _interruptable_sleep(self, expected_sleep, sleep_interval=0.5):
        """
        An interruptable sleep... Sleep in small steps (typically about
        1 second) checking if self._running is still True. If not,
        return early to break out of the loop.

        Required to prevent the UI from having to wait for a sleep to
        finish.

        @type expected_sleep: int | float
        @param expected_sleep: Expected time (in seconds) to sleep.

        @type sleep_interval: int
        @param sleep_interval: Interval (in seconds) to sleep for
            before checking if self._running is still still set to
            True.
        """
        wake_up = time() + expected_sleep

        while time() < wake_up:
            sleep(sleep_interval)
            if not self._running:
                wake_up = time()

    def start_kiosk(self):
        """
        Start the kiosk. Non-blocking call to prevent the UI from
        locking up when the 'start' button is pressed.

        @rtype: bool
        @return: True if the thread has successfully started, else
            False.
        """
        self._run_it_thread = Thread(
            target=self._run_it)
        self._run_it_thread.start()
        return self._run_it_thread.isAlive()

    def _run_it(self):
        """
        Run the Kiosk.
        """
        if not self._config.get('web_pages') and not len(self._config['web_pages']):
            print("'web_pages' not found in config.")
            exit(-1)

        self._running = True

        while self._running:
            for web_page in self._config['web_pages']:
                if not web_page.get('window_id'):
                    open_str = "window.open('{uri}');".format(uri=web_page['uri'])
                    self._driver.execute_script(open_str)

                    # assuming the newly created tab is the last in the list....
                    web_page['window_id'] = self._driver.window_handles[-1]

                self._driver.switch_to.window(web_page['window_id'])

                if web_page.get('reload', False):
                    # the line below seems to be causing issues, need investigating
                    # self.driver.get(web_page['uri'])
                    self._interruptable_sleep(
                        expected_sleep=int(web_page.get('reload_time', 5)))

                self._interruptable_sleep(
                    expected_sleep=int(web_page.get('display_for', 30)))

                # check if we need to break out of the loop earlier...
                # i.e. start/pause button has been pressed.
                if not self._running:
                    break

    def stop_kiosk(self):
        """
        Stop/Pause the rotation function of the kiosk.

        @rtype bool
        @return: Return True if loop thread has stopped, else False.
        """
        if self._run_it_thread is not None and self._running:
            self._running = False

            if self._run_it_thread.isAlive():
                self._run_it_thread.join()

        return not self._run_it_thread.isAlive()
